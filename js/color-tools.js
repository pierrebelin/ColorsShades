function rgbToHex(R,G,B) {
    return toHex(R)+toHex(G)+toHex(B)
}


function toHex(n) {
    n = parseInt(n,10);
    if (isNaN(n)){
        return "00";
    }
    n = Math.max(0,Math.min(n,255));

    return "0123456789ABCDEF".charAt((n-n%16)/16) + "0123456789ABCDEF".charAt(n%16);
}

function hextoRGB(hexa){
  return getRGBSyntax(hexToR(hexa), hexToG(hexa), hexToB(hexa));
}

function hexToR(h) {return parseInt((getHexaValue(h)).substring(0,2),16)}
function hexToG(h) {return parseInt((getHexaValue(h)).substring(2,4),16)}
function hexToB(h) {return parseInt((getHexaValue(h)).substring(4,6),16)}

function getRGBSyntax(r, g, b){
  return "rgb(" + r + "," + g + "," + b + ")";
}


function getHexaSyntax(hexa){
  return "#" + hexa;
}

function getHexaValue(hexa){
  return ((hexa.charAt(0)=="#") ? hexa.substring(1,7):hexa);
}
