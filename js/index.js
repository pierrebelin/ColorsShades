var hexaSelected = "";

$(document).ready(function(){
  $("#global-section").hide();
  if(getQueryVariable("color").length > 0){
    hexaSelected = getHexaValue(getQueryVariable("color"));

    $("#form_color_display").css('background-color', getHexaSyntax(hexaSelected));

    if(getQueryVariable("mode").length > 0){
      mode = getQueryVariable("mode");
      $("#div_form_mode").append('<select id="form_mode" onchange="submitColorForm()" required="required" class="form-control"></select>');
      switch(mode) {
        case "RGB":
        $("#form_mode").append('<option value="HEX">Hexadecimal - '+getHexaSyntax(hexaSelected)+'</option><option value="RGB"  selected="selected" >RGB  - '+hextoRGB(hexaSelected)+'</option>');
        break;
        default:
        $("#form_mode").append('<option value="HEX" selected="selected">Hexadecimal - '+getHexaSyntax(hexaSelected)+'</option><option value="RGB">RGB  - '+hextoRGB(hexaSelected)+'</option>');
      }
    }
    $("#global-section").show();
    calculateColors(hexaSelected);
  }

  $(".filter-button").click(function(){
    var value = $(this).attr('data-filter');

    if(value == "all")
    {
      $('.filter').show('1000');
    }
    else
    {
      $(".filter").not('.' + value).hide('3000');
      $('.filter').filter('.' + value).show('3000');
    }
  });

  $('#form_color').keypress(function(e) {
    if (e.which == 13) {
      submitColor();
      e.preventDefault();
    }
  });
  $('.popupunder').hide();
});

function submitColorForm(){
  var tmp = $("#div_form_mode option:selected").val();
  submitColor(tmp);
}


function popSuccessCopy(color){
  window.setTimeout(function() {
    $(".alert-text").text(color + " has been copied to clipboard");
    $(".popupunder").fadeTo(1500, 100).slideUp(400, function(){
      $('.popupunder').hide();
    });
    // 500 : Time will remain on the screen
  }, 500);
}
